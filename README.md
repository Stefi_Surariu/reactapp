This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Check folder 'printScreens'.

Objectives:

- Get a basic overview of JavaScript frameworks and libraries
- Understand the architecture of an React application
- Scaffold out a starter React application using create-react-app, the command line tool
- Construct the React component code and the view for your component using JSX and JavaScript
- Create presentational, container and functional components in your React application
- Set up the router module to enable navigation among multiple component views
- Set up the routes to enable the navigation
- Design Single Page Application using React
- Create uncontrolled forms through uncontrolled components in React
- Handle the form submission in the React application
- Install and Configure Redux in your application
- Enable your React app to make use of Redux
- Configure and use react-redux-form to create Controlled forms
- Store the form state in the Redux store
- Define Redux actions
- Create action creator functions that return action objects
- Split the reducer function into multiple simpler functions and combine the reducer functions
- Use Redux Thunk middleware to return a function instead of an action
- Use a logger middleware to print a log of actions initiated on the Redux store
- Set up a simple server that makes data available for clients
- Access the data from the server using a browser
- Use the json-server as a simple static web server
- Use Fetch to communicate from your React application with a REST API server
- Add animations using the react-transition-group and react-animation-components